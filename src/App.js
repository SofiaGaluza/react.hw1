import logo from './logo.svg';
import './App.css';
import React from "react";
import axios from "axios";

const baseURL = "https://localhost:5001/api/v1/Customers/a6c8c6b1-4349-45b0-ab31-244740aaf0f0";

export default function App() {
  const [post, setPost] = React.useState(null);

  React.useEffect(() => {
    axios.get(baseURL).then((response) => {
      setPost(response.data);
    });
  }, []);

  if (!post) return null;

  return (
    <div>
      <h1>{post.firstName}</h1>
      <h1>{post.lastName}</h1>
      <h1>{post.email}</h1>
    </div>
  );
}